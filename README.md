# Command Runner Bot

A Telegram bot that runs pre-defined commands.

## Setup
+ Install `python-telegram-bot`
    + `pip3 install python-telegram-bot`
+ Clone this repo (`git clone https://gitlab.com/blankX/command-runner-bot.git`)
+ cd into the repo's directory (`cd command-runner-bot`)
    + Copy `sample.config.py` to `config.py` (`cp sample.config.py config.py`)
    + Edit `config.py` with your text editor
+ If you want to, add files into the `commands` directory

## Start
+ `python3 bot.py &`

## Kangs
+ I kanged the code for user checking in [pc-monitor-bot](https://gitlab.com/ceda_ei/pc-monitor-bot)
+ I kanged the code for markdown in [Joker-dabot](https://github.com/Yasir-siddiqui/Joker_dabot)
