#!/usr/bin/env python3
import logging,telegram,subprocess,os
from os import listdir
from os.path import isfile, join
from telegram import ParseMode
from telegram.ext import Updater, CommandHandler
try:
	import config
except ImportError:
	print('config.py is missing, have you tried `cp sample.config.py config.py`?')
	exit()

sauce='https://gitlab.com/blankX/command-runner-bot'
cmds_wd=os.getcwd() + '/commands/'
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

def check(bot, update):
	chat_id = update.message.chat_id
	if not chat_id in config.its_them:
		try:
			global sauce
		except:
			logging.error('Cannot `global sauce`')
		bot.send_message(chat_id=chat_id, text='You need to host your own instance, source: ' + sauce)
		return False
	return True

def start(bot, update):
	if not check(bot, update):
		return False
	chat_id = update.message.chat_id
	bot.send_message(chat_id=chat_id, text='Heya, try /commands')

def commands(bot, update, args):
	if not check(bot, update):
		return False
	chat_id = update.message.chat_id
	try:
		cmd=' '.join(args)
	except:
		cmd=''
	try:
		global cmds_wd
	except:
		logging.error('Failed `global cmds_wd`')
	mypath=cmds_wd
	owo=[f for f in listdir(mypath) if isfile(join(mypath, f))]
	if '' == cmd:
		skep=', '.join(owo)
		py='How to use: /commands command\nAvaliable commands:\n'
		if skep != '':
			bot.send_message(chat_id=chat_id, text=py + '`' + skep + '`', parse_mode=ParseMode.MARKDOWN)
		else:
			bot.send_message(chat_id=chat_id, text=py + 'None.')
	else:
		if cmd in owo:
			bot.send_message(chat_id=chat_id, text='Running ' + cmd)
			the_thing='`' + subprocess.run(cmds_wd + cmd, stdout=subprocess.PIPE).stdout.decode() + '`'
			if the_thing == '``':
				bot.send_message(chat_id=chat_id, text='Done.')
			else:
				bot.send_message(chat_id=chat_id, text=the_thing, parse_mode=ParseMode.MARKDOWN)
		else:
			bot.send_message(chat_id=chat_id, text='`' + cmd + '` doesn\'t exist.', parse_mode=ParseMode.MARKDOWN)

updater = Updater(token=config.api_key)
dispatcher = updater.dispatcher

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

help_handler = CommandHandler('help', start)
dispatcher.add_handler(help_handler)

commands_handler = CommandHandler('commands', commands, pass_args=True)
dispatcher.add_handler(commands_handler)

updater.start_polling()
